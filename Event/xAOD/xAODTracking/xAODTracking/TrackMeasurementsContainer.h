/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

#ifndef XAODTRACKING_TRACKMEASUREMENTSCONTAINER_H
#define XAODTRACKING_TRACKMEASUREMENTSCONTAINER_H

#include "xAODTracking/versions/TrackMeasurementsContainer_v1.h"

namespace xAOD {
  typedef TrackMeasurementsContainer_v1 TrackMeasurementsContainer;
}

#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::TrackMeasurementsContainer , 1185958447 , 1 )
#endif